Briggs Law Firm Divorce Lawyers, founded by partners Larry Briggs, and his son Samuel Briggs, offer a variety of services and eagerly represent clients of every background. We pride ourselves in maintaining a small-firm feel by treating each case with care and consideration.


Address: 819 E North St, Greenville, SC 29601, USA

Phone: 864-777-9333

Website: https://briggslawfirm.com/
